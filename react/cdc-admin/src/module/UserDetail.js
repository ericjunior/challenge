import React, { Component } from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom';

class TabelaUserDetail extends Component 
{
  render() {
        return(
                <div>
                  <br />
                  Click the login to view detail of user.

                  <table className="pure-table">
                    <thead>
                      <tr>
                      <th>Id</th>
                      <th>Login</th>
                      <th>URL Perfil</th>
                      <th>Data Criação Login</th>
                      </tr>
                    </thead>
                    <tbody>
                        <tr key={this.props.lista.id}>
                          <td>{this.props.lista.id}</td>
                          <td>{this.props.lista.login}</td>
                          <td>{this.props.lista.html_url}</td>
                          <td>{this.props.lista.created_at}</td>
                          </tr>
                    </tbody>
                  </table>
                  <div><Link to={`/users`}>Go Back</Link></div>
                </div>
              );
            }
}


class TabelaUserRepo extends Component 
{
  render() {
        return(
                <div>
                  <br />
                  List of repositorie public

                  <table className="pure-table">
                    <thead>
                      <tr>
                      <th>Id</th>
                      <th>Nome</th>
                      <th>URL Repositório</th>
                      </tr>
                    </thead>
                    <tbody>
                    {
                      this.props.listaUserRepo.map(function(repo){
                      return (
                        <tr key={repo.id}>
                          <td>{repo.id}</td>
                          <td>{repo.name}</td>
                          <td><a href={`${repo.html_url}`} target="_blank" rel="noopener">{repo.html_url}</a></td>
                        </tr>
                      );
                    })
                  }
                    </tbody>
                  </table>
                  <div><Link to={`/users`}>Go Back</Link></div>
                </div>
              );
            }
}


export default class UserBox extends Component {
    constructor(props) {
        super(props);
        this.state = {lista : [], listaUserRepo:[]};
      }

      componentDidMount(){

        const {id} = this.props.match.params;    
          $.ajax({
            url: "https://challenge-nodejs2.herokuapp.com/api/users/"+id+"/details",
            dataType: 'json',
            success:function(resposta){    
              this.setState({lista:resposta});
            }.bind(this)
          }
        );

        $.ajax({
          url: "https://challenge-nodejs2.herokuapp.com/api/users/"+id+"/repos",
          dataType: 'json',
          success:function(resposta){    
            this.setState({listaUserRepo:resposta});
          }.bind(this)
        }
      );

      }

    render() {
        return(
            <div>
                    <div className="header">
                         <h1>List Users</h1>
                    </div>
                    <div className="content" id="content">
                        <TabelaUserDetail lista={this.state.lista}/>
                        <TabelaUserRepo   listaUserRepo={this.state.listaUserRepo}/>
                    </div>
            </div>
        );
    }
}