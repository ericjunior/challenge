import React, { Component } from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom';
import logo from '../Rolling-1s-73px.svg';

export default class UserBox extends Component 
{
    constructor(props) 
    {
        super(props);
        this.state = {lista : [], listPage : [0], nextPage : 0, shown : false };
    }

      handleClick(e) 
      {
        var _page = 0;
        this.setState({shown:true});


       if (e ==='back')
       {
          // Get the page
          if (this.state.listPage.length > 0){
            _page = this.state.listPage[this.state.listPage.length-2];
            var remove = this.state.listPage.splice(this.state.listPage.length-2,1);
          }
       }
       else {
        // update array the pages
        if (!this.state.listPage.includes(this.state.nextPage))
        this.state.listPage.push(this.state.nextPage);
        _page= this.state.nextPage;
       }
      
        // Recarrega a lista
        $.ajax({
          url: "https://challenge-nodejs2.herokuapp.com/api/users/"+ _page,
          dataType: 'json',
          success:function(resposta)
          {   
            var nextPage = resposta[resposta.length-1].link;
            this.setState({lista:resposta, nextPage: nextPage, shown:false});

          }.bind(this)});
      }

      componentDidMount()
      {
          //const {id} = this.props.match.params;
          
          this.setState({shown:true});

          $.ajax({
            url: "https://challenge-nodejs2.herokuapp.com/api/users/0",
            dataType: 'json',
            success:function(resposta)
            {   
              // get de next page
              var nextPage = resposta[resposta.length-1].link;
              this.setState({lista:resposta, nextPage: nextPage, shown: false});

            }.bind(this),
            error: function (jqXHR, exception) {
              var msg = '';
              if (jqXHR.status === 0) {
                  msg = 'Not connect.\n Verify Network.';
              } else if (jqXHR.status === 404) {
                  msg = 'Requested page not found. [404]';
              } else if (jqXHR.status === 500) {
                  msg = 'Internal Server Error [500].';
              } else if (exception === 'parsererror') {
                  msg = 'Requested JSON parse failed.';
              } else if (exception === 'timeout') {
                  msg = 'Time out error.';
              } else if (exception === 'abort') {
                  msg = 'Ajax request aborted.';
              } else {
                  msg = 'Uncaught Error.\n' + jqXHR.responseText;
              }
              console.log(msg);
            }
          }
        );
      }

    render() {
        var shown = {
			display: this.state.shown ? "block" : "none"
		};
		
		var hidden = {
			display: this.state.shown ? "none" : "block"
        };
        
        return(
            <div>
                    <div className="header">
                         <h1>List Users</h1>
                    </div>
                    
                    <div className="header" style={ shown }>
                         <img src={logo}  alt="loading" /> loading... 
                    </div>

                    <div className="content" id="content" style={ hidden }>
                            <br/>
                            Click the name of the user to request your information 
                            <br/>
                            <div>
                                    <button onClick={(e) => this.handleClick('back')}>Back Page</button>
                                    <button onClick={(e) => this.handleClick('next')}>Next Page </button>
                            </div>
                            <table className="pure-table">    
                            <thead>
                            <tr>
                                <th>Id</th>
                                <th>Login</th>
                            </tr>
                            </thead>
                            <tbody>                  
                            {this.state.lista.slice(0,-1).map(function(user){
                                return (
                                        <tr key={user.id}>
                                            <td>{user.id}</td>
                                            <td><Link to={{ pathname:`/user/${user.login}/detail` }} className="pure-menu-link">{user.login}</Link></td>
                                        </tr>
                                    );
                                })
                            }
                            </tbody>
                        </table>
                    </div>
            </div>
        );
    }
}