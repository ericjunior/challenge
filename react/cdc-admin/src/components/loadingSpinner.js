import React from 'react';
import logo from '../Rolling-1s-73px.svg';

export default class loadingSpinner extends React.Component {
  render() {
    var shown = {
			            display: this.state.shown ? "block" : "none"
		            };
		
		var hidden = {
			            display: this.state.shown ? "none" : "block"
                };

      return (
        <div className="header" style={ shown }>
            <img src={logo}  alt="loading" /> loading... 
        </div>
      ); 
  }
}