import React, { Component } from 'react';

export default class Home extends Component {

  componentDidMount() {  }

  render() {
    return (
      <div>
        <div className="header">
          <h1>Welcome to Challenge - shawandpartners.com  </h1>
        </div>
        <div className="content" id="content">

          <h3>Challenge instructions</h3>
          <p> You must provide a link for your live app, we suggest Heroku, there you can publish your application for free.</p>
          <p> You have to create a private repository for your code, you can use Bitbucket it allows you to create private repositories for free, but feel free to use any git repository that you prefer, the only requirement is that it must be private.</p>
          <p> When you finish the challenge please share the app repository with <strong>lucas@shawandpartners.com</strong> so we will be able to check your code.</p>

          <h3>Rules</h3>

          <p> Each step of the test can have the following tags <strong>[full-stack]</strong>,
              <strong>[back-end]</strong> and/or <strong>[Front-end]</strong>.</p>

          <dl><dt>[full-stack]</dt>
            <dd> If you applied for the full-stack role you must implement this step</dd><dt>[back-end]</dt>
            <dd> If you applied for the back-end role you must implement this step</dd><dt>[front-end]</dt>
            <dd> If you applied for the front-end role you must implement this step</dd></dl>
          <p> You can use any back-end language/framework but NodeJS is very appreciated. For the front-end you must use a framework (React, Angular, Vue, Preact, etc..) but React is very appreciated as well.</p>
          <p> Pay attention to your configurations and dependencies, projects that do not compiles or does not boot up will be ignored.</p>
          <p> If your project demands any custom configuration, you have to provide a README.md file with the instructions to execute your application.</p>
          <h3>On this test</h3>
          <p> You will have to consume the GitHub APIs, more precisely the users and repositories endpoints, and create an application.</p>
          <p>The endpoints documentantion that you will use for this test are available at:</p>

          <ul><li> <a href="https://developer.github.com/v3">https://developer.github.com/v3/</a></li>
            <li> <a href="https://developer.github.com/v3/users">https://developer.github.com/v3/users/</a></li>
            <li> <a href="https://developer.github.com/v3/repos">https://developer.github.com/v3/repos/</a></li></ul>
            
            <h3>STEPS</h3><ol><li> <strong>[full-stack] [back-end]</strong>
            <p> Create an API that will proxy all client requests to the appropriate GitHub endpoint. The following endpoints must be provided:</p>
            <ul>
              <li> <strong>GET - /api/users?since=number</strong>
              <p> This endpoint must return a list of GitHub users and the link for the next page.</p></li>
              <li> <strong>GET - /api/users/:username/details</strong><p> This endpoint must return the details of a GitHub user</p></li>
              <li> <strong>GET - /api/users/:username/repos</strong><p> This endpoint must return a list with all user repositories</p>
              </li></ul></li><li> <strong>[full-stack (optional)] [back-end]</strong><p> Create tests for your application covering all endpoints.</p></li><li> <strong>[full-stack] [front-end]</strong><p> Create a screen that will list all users from GitHub and will display their Id and Login. Remember, you can’t list all users at once you will have to use pagination.</p><p> (if you are doing this test for the front-end role you have to consume the GitHub endpoints directly)</p><p> (if you are doing this test for the full stack role you must consume the endpoints that you created at step 1. )</p></li><li> <strong>[full-stack] [front-end]</strong><p> Using the screen created in step 3, create a new screen that will display the details of a user (Id, Login, Profile URL and the date of the login creation).</p></li><li> <strong>[full-stack] [front-end]</strong><p> On the details screen (created on step 4) add a table that will display the public repositories of the user, this table must contain the id, name, and URL to the repository.</p></li></ol><h2>Have fun!! 🙂</h2>

        </div>
      </div>
    );
  }
}