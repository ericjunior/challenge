import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import UserBox from './module/User';
import UserDetail from './module/UserDetail';
import Home from './Home';

import registerServiceWorker from './registerServiceWorker';
import {BrowserRouter as Router, Route,Switch} from 'react-router-dom';

ReactDOM.render((
    <Router>
        <App>
            <Switch>            
                <Route exact path="/" component={Home}/>
                <Route path="/users" component={UserBox}/>     
                <Route path="/users/:id" component={UserBox}/>   
                <Route exact path="/user/:id/detail" component={UserDetail}/>
            </Switch>            
        </App>
    </Router>

), document.getElementById('root'));
  registerServiceWorker();
