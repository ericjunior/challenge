import React, { Component } from 'react';
import './css/pure-min.css';
import './css/side-menu.css';
import { Link } from 'react-router-dom';

class App extends Component 
{

  // antes de chamar o render
  componentWillMount()
  {}

  // logo apos a chamada do render
  componentDidMount()
  {}

  render() {    
    return (
      <div id="layout">

          <a href="" id="menuLink" className="menu-link">

              <span></span>
          </a>

          <div id="menu">
              <div className="pure-menu">
                  <div className="pure-menu-heading" href="">Menu</div>

                  <ul className="pure-menu-list">
                      <li className="pure-menu-item"><Link to="/" className="pure-menu-link">Home</Link></li>
                      <li className="pure-menu-item"><Link to="/users" className="pure-menu-link">Users</Link></li>
                  </ul>
              </div>
          </div>

              <div id="main">
                {this.props.children}
              </div>            


      </div>     
    );
  }
}

export default App;
