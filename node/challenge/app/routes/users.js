// ------------------------------------------------------------------------------
// Author       : Eric Nilson Moreira Junior
// Created      : 22/08/2018
// Description  : Module responsible for providing user information from gitihub
// ------------------------------------------------------------------------------
module.exports = function (app) {

    var https = require('https');
    
    var _url;
    var _since;

    //GET - /api/users?since={number}
    //This endpoint must return a list of GitHub users and the link for the next page.
    app.get('/api/users/:since', function (req, res) {
        
        var _since = (req.params.since);
        var _url = "/users?since=" + _since;
        _since = "OK";

        getDataFromGithub(_url, _since).then(function (result) 
        {
            res.json(result);
        })
            .catch(function () {
                res.send("error during execution. status code: " + res.statusCode);
            });
    });

    //GET - /api/users/:username/details
    //This endpoint must return the details of a GitHub user
    app.get('/api/users/:username/details', function(req, res) {
        
        var _username = (req.params.username);
        var _url = "/users/" + _username;

        getDataFromGithub(_url, "NOK").then(function (result) 
        {
            res.json(result);
        })
            .catch(function () {
                res.send("error during execution. status code: " + res.statusCode);
            });
    });

    //GET - /api/users/:username/repos
    //This endpoint must return a list with all user repositories
    app.get('/api/users/:username/repos', function(req, res) {
       
        var _username = (req.params.username);
        var _url = "/users/" + _username + "/repos";

        getDataFromGithub(_url, "NOK").then(function (result) 
        {
            res.json(result);
        })
            .catch(function () {
                res.send("error during execution. status code: " + res.statusCode);
            });

      });


    // Function for get data from Github.
    function getDataFromGithub(path, _since) {

        var myPromise = new Promise(function (resolve, reject) {

            var optionsget = {
                host: 'api.github.com', 
                path: path,
                method: 'GET',
                headers: { 'user-agent': 'node.js' }
            };

            var reqGet = https.request(optionsget, function (response) {

                var str = '';

                response.on('data', function (chunk) {
                    str += chunk;
                });

                response.on('end', function () 
                {
                    if (_since === "OK")
                    {
						// get the Link Header from GitHub
						strHeaderResponse = JSON.stringify(response.headers.link); 
						
                        
                        if (strHeaderResponse != null ){
                            // calc to get only since number next page
                            var strFindSince 	= strHeaderResponse.indexOf("since");
                            var strFindBar 		= strHeaderResponse.indexOf(">;");
                            
                            var strlenNextPage 	= (strFindBar - (strFindSince + 6));	
                            
                            var obj = JSON.parse(str);
                            
                            var strNextPage = ({"id" : 0, "link": strHeaderResponse.substr(37, strlenNextPage), "Description" : "Add to link header from github - next page to pagination"});
                            
                            
                            obj.push(strNextPage);						
                            
                            resolve(obj);
                        }
                        else
                        {
                            reject("error");
                        }
                    
                    }
                    else
                    {
                        var obj = JSON.parse(str);
                        resolve(obj);
                    }

                });
            });
            reqGet.end();
            
        });

        return myPromise;
    }
}
