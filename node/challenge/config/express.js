
    
    var express = require('express');
    var consign = require('consign');
    var bodyParser = require('body-parser');
    //var expressValidator = require('express-validator');

    module.exports = function(){     
        var app = express();
    
        //app.use(express.static('./public'));
        //app.set('view engine', 'ejs');
        //app.set('views','./app/views');

        app.set('view engine', 'ejs');
        app.set('views','./app/views');

        app.use(function(req, res, next) {
            res.header("Access-Control-Allow-Origin", '*');
            res.header("Access-Control-Allow-Credentials", true);
            res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
            res.header("Access-Control-Allow-Headers", 'Origin,X-Requested-With,Content-Type,Accept,content-type,application/json');
            
            next();
        });
        
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        //app.use(expressValidator());

        consign()
            .include('./app/routes')
            .into(app);  

        return app;
    }
